### :point_right: This starter repo has moved to the [ionic-team/starters](https://github.com/ionic-team/starters/tree/master/ionic-angular/official/sidemenu) repo! :point_left:

# Setup Environments

## Preparation

- Pastikan anda menggunakan mesin UNIX. Untuk saat ini windows dan sebangsanya belum disupport.
- Pastikan mesin anda sudah terinstall Docker Engine.
- Pastikan untuk ios dan android sudah terinstall semua support environment. Android SDK dan xCode.

# Code Convention

Halo developers!, selamat datang di devetek platform team. Kami berusaha membuat source code yang kita gunakan semudah mungkin bagi para developer. Di bagian ini akan kami jelaskan rules untuk melakukan coding di project Hompes Platform.

## Environment Requirements
- IDE: Visual Studio Code
- Plugin
      - [tslint](https://github.com/Microsoft/vscode-typescript-tslint-plugin)

## Project Structure
```sh
root/
├── config                          # All environment config
├── docker                          # Docker compose for all environments
├── resources                       # Assets for all platforms (ios/android/desktop)
├── src                             # Root App Folders
      ├── app                       # App core modules initialize
      ├── assets                    # App static assets
      ├── models                    # App model struct
      ├── pages                     # App module page
      ├── providers                 # App module provider (auth/http/hash/encrypt/etc)
      ├── settings                  # App local config by provider
      ├── shared                    # App shared helper (errorHandler/etc)
      ├── theme                     # App installed theme
├── tools                           # Collection scripts for CI, Webpack, and others
```

# Command Generator

## How To Create Page
```sh
TODO: Will update as soon as possible!
```

## How To Create Model
```sh
TODO: Will update as soon as possible!
```

# Running Development

```sh
make run-dev
```

# Build iOS Platform
Jalankan xCode secara otomatis, dengan eksekusi command di bawah. Kemudian jalankan aplikasi atau build aplikasi.

```sh
make run-build-ios
```

# Build Android Platform
Jalankan Android Studio secara otomatis, dengan eksekusi command di bawah. Kemudian jalankan aplikasi atau build aplikasi.

```sh
make run-build-android
```


# References
- Mengaktifkan ionic project menggunakan [capacitor](https://capacitor.ionicframework.com/docs/getting-started/with-ionic) 
- Merubah splashscreen dan icon launcher di [capacitor](https://www.joshmorony.com/adding-icons-splash-screens-launch-images-to-capacitor-projects/)
- Update iOS dependencies for [capacitor](https://guides.cocoapods.org/using/getting-started.html)