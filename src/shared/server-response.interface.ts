

export interface ServerResponseArray<T> {
    data: T[];
    error: string[];
    message: string;
    success?: boolean;
}

export interface ServerResponseSingle<T> {
    data: T;
    error: string[];
    message: string;
    success?: boolean;
}
