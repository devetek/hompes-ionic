import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, Injector, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { MyApp } from './app.component';
import { BookmarkedPage } from '@pages/bookmarked/bookmarked';
import { CategoryPage } from '@pages/category/category';
import { CourcedetailsPage } from '@pages/courcedetails/courcedetails';
import { CoursePage } from '@pages/course/course';
import { DesignPage } from '@pages/design/design';
import { FiltersPage } from '@pages/filters/filters';
import { HomePage } from '@pages/home/home';
import { LecturerprofilePage } from '@pages/lecturerprofile/lecturerprofile';
import { MyprofilePage } from '@pages/myprofile/myprofile';
import { NotificationPage } from '@pages/notification/notification';
import { OngoingcoursesPage } from '@pages/ongoingcourses/ongoingcourses';
import { SearchPage } from '@pages/search/search';
import { SignupPage } from '@pages/signup/signup';
import { SinginPage } from '@pages/singin/singin';
import { SupportPage } from '@pages/support/support';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '@providers/auth/auth';
import { UserProvider } from '@providers/user/user';
import { IonicStorageModule } from '@ionic/storage';
import { PacketProvider } from '@providers/packet/packet';
import { PacketCategoryProvider } from '@providers/packet-category/packet-category';
import { HttpModule } from '@angular/http';
import { InterceptorProvider } from '@providers/interceptor/interceptor';
import { Help } from '@shared/help';
import { PacketBookProvider } from '@providers/packet-book/packet-book';
import { MypacketProvider } from '@providers/mypacket/mypacket';
import { MyCourcedetails } from '@pages/myCourcedetails/myCourcedetails';
import { PacketRequirementsProvider } from '@providers/packet-requirements/packet-requirements';
import { PacketDetailsProvider } from '@providers/packet-details/packet-details';
import { PacketGoalsProvider } from '@providers/packet-goals/packet-goals';
import { BookProvider } from '@providers/book/book';
import { CourseReadingPage } from '@pages/courseReading/courseReading';
import { BookContentProvider } from '@providers/book-content/book-content';
import { BookContentTicketProvider } from '@providers/book-content-ticket/book-content-ticket';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        MyApp,
        BookmarkedPage,
        CategoryPage,
        CourcedetailsPage,
        CoursePage,
        DesignPage,
        FiltersPage,
        HomePage,
        LecturerprofilePage,
        MyprofilePage,
        NotificationPage,
        OngoingcoursesPage,
        SearchPage,
        SignupPage,
        SinginPage,
        SupportPage,
        MyCourcedetails,
        CourseReadingPage
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        HttpClientModule,
        HttpModule,
        IonicStorageModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        BookmarkedPage,
        CategoryPage,
        CourcedetailsPage,
        CoursePage,
        DesignPage,
        FiltersPage,
        HomePage,
        LecturerprofilePage,
        MyprofilePage,
        NotificationPage,
        OngoingcoursesPage,
        SearchPage,
        SignupPage,
        SinginPage,
        SupportPage,
        MyCourcedetails,
        CourseReadingPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        AuthProvider,
        UserProvider,
        Help,
        PacketProvider,
        PacketCategoryProvider,
        { provide: HTTP_INTERCEPTORS, useClass: InterceptorProvider, multi: true },
        PacketBookProvider,
        MypacketProvider,
        PacketRequirementsProvider,
        PacketDetailsProvider,
        PacketGoalsProvider,
        BookProvider,
        BookContentProvider,
        BookContentTicketProvider
    ]
})
export class AppModule {
    static injector: Injector;

    constructor(injector: Injector) {
        AppModule.injector = injector;
    }
}
