import { AuthProvider } from '@providers/auth/auth';
import { UserProvider } from '@providers/user/user';
import { User } from '@models/user';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MyprofilePage } from '@pages/myprofile/myprofile';
import { OngoingcoursesPage } from '@pages/ongoingcourses/ongoingcourses';
import { BookmarkedPage } from '@pages/bookmarked/bookmarked';
import { SupportPage } from '@pages/support/support';
import { SinginPage } from '@pages/singin/singin';
import { HomePage } from '@pages/home/home';
import { NotificationPage } from '@pages/notification/notification';
import { TranslateService } from '@ngx-translate/core';


@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;
    currentUser: User;
    rootPage: any;
    loading: any;

    pages: Array<{ title: string, component: any }>;

    constructor(
        private platform: Platform,
        private statusBar: StatusBar,
        private splashScreen: SplashScreen,
        private loadingCtrl: LoadingController,
        public translate: TranslateService,
        public userProv: UserProvider,
        private authService: AuthProvider,
        private alertCtrl: AlertController,
    ) {
        this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(async () => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.translate.setDefaultLang('en');
            this.translate.use('en');

            try {
                this.authService.checkIsAuthenticated().then(isAuthenticated => {
                    if (isAuthenticated) {
                        this.userProv.getUser().subscribe(value => this.currentUser = value);
                        this.rootPage = HomePage;

                        return;
                    }

                    this.rootPage = SinginPage;
                });
            } catch (err) {
                const alert = this.alertCtrl.create({ title: 'Error', message: 'Error on verify authentication info', buttons: ['Ok'] });
                alert.present();
                this.rootPage = SinginPage;
            }
        });
    }

    openPage(page: { component: any; }) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.loading.present();
        this.nav.setRoot(page.component);
    }

    private __setPage(selectedPage: any) {
        this.nav.setRoot(selectedPage);
        this.loading.dismiss();
    }

    /**
     * List of routes
     * Set routes for function, navigation or direct/redirection
     */

    home() {
        this.__setPage(HomePage);
    }

    myprofile() {
        this.__setPage(MyprofilePage);
    }

    notification() {
        this.__setPage(NotificationPage);
    }

    ongoingcourses() {
        this.__setPage(OngoingcoursesPage);
    }

    bookmarked() {
        this.__setPage(BookmarkedPage);
    }

    support() {
        this.__setPage(SupportPage);
    }

    singin() {
        this.__setPage(SinginPage);
    }
}
