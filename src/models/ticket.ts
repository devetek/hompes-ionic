export interface Ticket {
    id_ticket: number;
    id_purchase?: any;
    for_module: string;
    module_id: number;
    sub_module_id: number;
    id_user: number;
    title_ticket: string;
    ticket_content: string;
    date_publish?: any;
    date_close?: any;
    ticket_status: string;
    id_publisher_close?: any;
    id_user_close?: any;
    note_for_close?: any;
    validasi: number;
    created_at: string;
    updated_at: string;
    name: string;
}
