export interface Packet_goal {
    id_packet_goal: number;
    id_packet: number;
    order_goal: number;
    goal: string;
    icon: string;
    verified_user: string;
    validasi: number;
    created_at: string;
    updated_at: string;
}
