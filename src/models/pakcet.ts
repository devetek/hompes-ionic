export interface Packets {
    id_packet: number;
    name_packet: string;
    excerpt_packet: string;
    description_packet: string;
    id_publisher_team?: any;
    cover_packet: string;
    thum_packet: string;
    date_publish?: any;
    id_company: number;
    status_packet: string;
    is_public: string;
    price: number;
    validasi: number;
    created_at: string;
    updated_at: string;
    id_packet_sale: number;
    promo_text?: any;
    price_packet_sale: number;
    time_support: number;
    phone_company: string;
    name_company: string;
    about_company: string;
}
