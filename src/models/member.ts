export interface Member {
    id_member: number;
    id_company: number;
    first_name: string;
    last_name: string;
    tgl_lahir?: any;
    last_education: string;
    phone_member: string;
    poscode_member: string;
    email_member: string;
    foto_member: string;
    thum_member: string;
    address_member: string;
    status_member: string;
    register_form: string;
    validasi: number;
    id_user: number;
    id_user_rgister: number;
    created_at?: any;
    updated_at: string;
}
