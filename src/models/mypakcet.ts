export interface Support {
    id_member_subscribe: number;
    module_id: number;
    date_accept: string;
    date_purchase: string;
    price_purchase: number;
    amount_paid: number;
    date_end: string;
    id_packet: number;
    status: string;
    status_support: number;
}

export interface Mypakcet {
    id_packet: number;
    name_packet: string;
    excerpt_packet: string;
    description_packet: string;
    id_publisher_team?: any;
    cover_packet: string;
    thum_packet: string;
    date_publish: string;
    id_company: number;
    status_packet: string;
    is_public: string;
    price: number;
    validasi: number;
    created_at: string;
    updated_at: string;
    id_packet_sale: number;
    promo_text?: any;
    price_packet_sale: number;
    time_support: number;
    phone_company: string;
    name_company: string;
    about_company: string;
    id_member_subscribe: number;
    amount_paid: number;
    date_active: string;
    id_member: number;
    support: Support[];
}
