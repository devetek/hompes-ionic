export interface User {
    id: number;
    name: string;
    email: string;
    email_verified_at?: any;
    created_at: string;
    updated_at: string;
    id_division?: any;
    id_from_division?: any;
    id_group?: any;
}