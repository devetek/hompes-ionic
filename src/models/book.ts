export interface Book {
    id_book: number;
    title_book: string;
    org_price: number;
    actual_price: number;
    description: string;
    promotional_text?: any;
    id_publisher: number;
    id_user: number;
    cover_url: string;
    thum_cover_url: string;
    date_publish: string;
    status: string;
    validasi: number;
    created_at: string;
    updated_at: string;
    first_name: string;
    foto_publisher: string;
    last_name: string;
    phone_number: string;
    public_name: string;
    title_publisher: string;
    user_input: string;
    about_company: string;
    name_company: string;
    logo_company?: any;
    address_company: string;
    phone_company: string;
    id_company: number;
    id_packet_book: number;
    id_packet: number;
}

export interface Content {
    order_book_content: number;
    time_book_content: number;
    id_book_content: number;
    title_book_content: string;
}

export interface Section {
    id_book_section: number;
    id_book: number;
    order_book_section: number;
    book_section: string;
    description_book_section: string;
    id_user: number;
    validasi: number;
    created_at: string;
    updated_at: string;
    content: Content[];
}

export interface BookRespon {
    book: Book;
    sections: Section[];
}
