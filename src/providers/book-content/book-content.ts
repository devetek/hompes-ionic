import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Help } from '@shared/help';
import { Observable } from 'rxjs';
import { ServerResponseSingle } from '@shared/server-response.interface';
import { Content } from '@models/book';
import { Service } from '@settings/Laravel';

/*
  Generated class for the BookContentProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BookContentProvider {

    constructor(public http: HttpClient, public errorH: Help) {
        console.log('Hello MypacketProvider Provider');
    }

    get(id_book_content): Observable<ServerResponseSingle<Content>> {
        return this.http
            .get(`${Service.apiUrl}/v1/book/client/contents/` + id_book_content)
            .map((value: ServerResponseSingle<Content>) => {
                return value;
            })
            .catch(this.errorH.handleError);
    }

}
