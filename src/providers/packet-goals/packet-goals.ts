import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Help } from '@shared/help';
import { Observable } from 'rxjs';
import { ServerResponseArray } from '@shared/server-response.interface';
import { Service } from '@settings/Laravel';
import { Packet_goal } from '@models/packet_goal';

/*
  Generated class for the PacketGoalsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PacketGoalsProvider {

    constructor(public http: HttpClient, public errorH: Help) {
        console.log('Hello MypacketProvider Provider');
    }

    get(param: any): Observable<ServerResponseArray<Packet_goal>> {
        return this.http
            .get(`${Service.apiUrl}/v1/packet/client/packet_goals`, { params: param })
            .map((value: ServerResponseArray<Packet_goal>) => {
                return value;
            })
            .catch(this.errorH.handleError);
    }

}
