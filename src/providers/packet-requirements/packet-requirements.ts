import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Help } from '@shared/help';
import { Observable } from 'rxjs';
import { ServerResponseArray } from '@shared/server-response.interface';
import { Service } from '@settings/Laravel';
import { Packet_requirement } from '@models/packet_requirement';

/*
  Generated class for the PacketRequirementsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PacketRequirementsProvider {

    constructor(public http: HttpClient, public errorH: Help) {
        console.log('Hello MypacketProvider Provider');
    }

    get(param: any): Observable<ServerResponseArray<Packet_requirement>> {
        return this.http
            .get(`${Service.apiUrl}/v1/packet/client/packet_requirements`, { params: param })
            .map((value: ServerResponseArray<Packet_requirement>) => {
                return value;
            })
            .catch(this.errorH.handleError);
    }

}
