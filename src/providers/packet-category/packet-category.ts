import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import { Service } from '@settings/Laravel';
import { Storage } from '@ionic/storage';
import { AuthProvider } from '../auth/auth';
import { ServerResponseArray } from '@shared/server-response.interface';

/*
  Generated class for the PacketCategoryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export interface PacketCategory {
    id_packet_master_category: number;
    name_category: string;
    description_category: string;
    validasi?: any;
    id_company: number;
    id_user?: any;
    created_at: string;
    updated_at?: any;
    cover_category: string;
}

@Injectable()
export class PacketCategoryProvider {

    baseUrl = 'https://jsonplaceholder.typicode.com';

    headers: HttpHeaders;
    options: any;

    constructor(
        public http: HttpClient,
        public storage: Storage,
        public auth: AuthProvider
    ) { }


    private handleError(error: any) {
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }


    get(): Observable<ServerResponseArray<PacketCategory>> {
        return this.http
            .get(`${Service.apiUrl}/v1/packet/client/packet_categories`)
            .map((value: ServerResponseArray<PacketCategory>) => {
                return value;
            })
            .catch(this.handleError);
    }


}
