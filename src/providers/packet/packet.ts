import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ServerResponseArray, ServerResponseSingle } from '@shared/server-response.interface';
import { Service } from '@settings/Laravel';
import { Help } from '@shared/help';
import { Packets } from '@models/pakcet';

/*
  Generated class for the PacketProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/



@Injectable()
export class PacketProvider {

    constructor(
        public http: HttpClient,
        public errorH: Help
    ) {
        console.log('Hello PacketProvider Provider');
    }

    get(param): Observable<ServerResponseArray<Packets>> {
        return this.http
            .get(`${Service.apiUrl}/v1/packet/client/packets`, { params: param })
            .map((value: ServerResponseArray<Packets>) => {
                return value;
            })
            .catch(this.errorH.handleError);
    }
    detail(id): Observable<ServerResponseSingle<any>> {
        return this.http
            .get(`${Service.apiUrl}/v1/packet/client/packets/` + id)
            .map((value: ServerResponseSingle<any>) => {
                return value;
            })
            .catch(this.errorH.handleError);
    }

    mydetail(id): Observable<ServerResponseSingle<any>> {
        return this.http
            .get(`${Service.apiUrl}/v1/packet/client/my_packets/` + id)
            .map((value: ServerResponseSingle<any>) => {
                return value;
            })
            .catch(this.errorH.handleError);
    }

}
