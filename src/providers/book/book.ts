import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Help } from '@shared/help';
import { Observable } from 'rxjs';
import { ServerResponseSingle } from '@shared/server-response.interface';
import { Service } from '@settings/Laravel';
import { BookRespon } from '@models/book';

/*
  Generated class for the BookProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BookProvider {

    constructor(public http: HttpClient, public errorH: Help) {
    }

    get(id_book: number): Observable<ServerResponseSingle<BookRespon>> {
        return this.http
            .get(`${Service.apiUrl}/v1/book/client/books/` + id_book)
            .map((value: ServerResponseSingle<BookRespon>) => {
                return value;
            })
            .catch(this.errorH.handleError);
    }

}
