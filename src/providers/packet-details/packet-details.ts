import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Help } from '@shared/help';
import { Observable } from 'rxjs';
import { ServerResponseArray } from '@shared/server-response.interface';
import { Service } from '@settings/Laravel';
import { Packet_detail } from '@models/packet_detail';

/*
  Generated class for the PacketDetailsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PacketDetailsProvider {


    constructor(public http: HttpClient, public errorH: Help) {
        console.log('Hello MypacketProvider Provider');
    }

    get(param: any): Observable<ServerResponseArray<Packet_detail>> {
        return this.http
            .get(`${Service.apiUrl}/v1/packet/client/packet_details`, { params: param })
            .map((value: ServerResponseArray<Packet_detail>) => {
                return value;
            })
            .catch(this.errorH.handleError);
    }

}
