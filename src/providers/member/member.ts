import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Help } from '@shared/help';
import { Observable } from 'rxjs';
import { ServerResponseSingle } from '@shared/server-response.interface';
import { Service } from '@settings/Laravel';
import { Member } from '@models/member';

/*
  Generated class for the MemberProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MemberProvider {

    constructor(
        public http: HttpClient,
        public errorH: Help
    ) {

    }

    get(): Observable<ServerResponseSingle<Member>> {
        return this.http
            .get(`${Service.apiUrl}/v1/member/client/members`)
            .map((value: ServerResponseSingle<Member>) => value)
            .catch(this.errorH.handleError);
    }

    put(id: number, data: Member): Observable<ServerResponseSingle<Member>> {
        return this.http
            .put(`${Service.apiUrl}/v1/member/client/members/${id.toString()}`, { params: data })
            .map((value: ServerResponseSingle<Member>) => value)
            .catch(this.errorH.handleError);
    }
}
