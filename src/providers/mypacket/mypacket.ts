import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ServerResponseArray } from '@shared/server-response.interface';
import { Service } from '@settings/Laravel';
import { Mypakcet } from '@models/mypakcet';
import { Help } from '@shared/help';

/*
  Generated class for the MypacketProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MypacketProvider {

    constructor(public http: HttpClient, public errorH: Help) {
        console.log('Hello MypacketProvider Provider');
    }

    get(param): Observable<ServerResponseArray<Mypakcet>> {
        return this.http
            .get(`${Service.apiUrl}/v1/packet/client/my_packets`, { params: param })
            .map((value: ServerResponseArray<Mypakcet>) => {
                return value;
            })
            .catch(this.errorH.handleError);
    }


}
