import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';

import { User } from '@models/user';
import { Service } from '@settings/Laravel';

@Injectable()
export class UserProvider {
  constructor(public http: HttpClient, private storage: Storage) { }

  async getUserInfo() {
    let auth: any = await this.storage.get('auth');
    let headers: HttpHeaders = new HttpHeaders({
      'Authorization': `Bearer ${auth.access_token}`,
    });

    return this.http.get(`${Service.apiUrl}/user`, { headers }).toPromise();
  }

  getUser(): Observable<User> {
    return this.http.get(`${Service.apiUrl}/user`).map(
      (value: User) => {
        return value;
      }
    );
  }
}
