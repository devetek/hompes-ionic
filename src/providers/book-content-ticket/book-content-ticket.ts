import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Help } from '@shared/help';
import { Observable } from 'rxjs';
import { ServerResponseArray } from '@shared/server-response.interface';
import { Service } from '@settings/Laravel';
import { Ticket } from '@models/ticket';

/*
  Generated class for the BookContentTicketProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BookContentTicketProvider {

    constructor(public http: HttpClient, public errorH: Help) {
        console.log('Hello MypacketProvider Provider');
    }

    get(param): Observable<ServerResponseArray<Ticket>> {
        return this.http
            .get(`${Service.apiUrl}/v1/ticket/client/tickets`, { params: param })
            .map((value: ServerResponseArray<Ticket>) => {
                return value;
            })
            .catch(this.errorH.handleError);
    }

}
