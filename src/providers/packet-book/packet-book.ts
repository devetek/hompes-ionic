import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ServerResponseArray, ServerResponseSingle } from '@shared/server-response.interface';
import { Service } from '@settings/Laravel';
import { Packet_book } from '@models/packet_book';
import { Help } from '@shared/help';

/*
  Generated class for the PacketBookProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PacketBookProvider {

    constructor(public http: HttpClient, public errorH: Help) {
        console.log('Hello PacketBookProvider Provider');
    }

    get(param: any): Observable<ServerResponseArray<Packet_book>> {
        return this.http
            .get(`${Service.apiUrl}/v1/packet/client/packet_books`, { params: param })
            .map((value: ServerResponseArray<Packet_book>) => {
                return value;
            })
            .catch(this.errorH.handleError);
    }

    detail(id: string): Observable<ServerResponseSingle<any>> {
        return this.http
            .get(`${Service.apiUrl}/v1/packet/client/packet_books/` + id)
            .map((value: ServerResponseSingle<any>) => {
                return value;
            })
            .catch(this.errorH.handleError);
    }

}
