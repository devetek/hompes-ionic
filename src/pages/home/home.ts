import { Component } from '@angular/core';
import { NavController, ModalController, LoadingController } from 'ionic-angular';

import { SearchPage } from '@pages/search/search';
import { CategoryPage } from '@pages/category/category';
import { NotificationPage } from '@pages/notification/notification';
import { PacketCategory, PacketCategoryProvider } from '@providers/packet-category/packet-category';
import { OngoingcoursesPage } from '@pages/ongoingcourses/ongoingcourses';
import { MypacketProvider } from '@providers/mypacket/mypacket';
import { Mypakcet } from '@models/mypakcet';
import { MyCourcedetails } from '@pages/myCourcedetails/myCourcedetails';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    packetCategory: PacketCategory[] = [];
    dataMyPacket: Mypakcet[] = [];
    loading: any;

    constructor(
        public navCtrl: NavController,
        public modalCtrl: ModalController,
        private loadingCtrl: LoadingController,
        public myPacketProv: MypacketProvider,
        public packet_category: PacketCategoryProvider,
    ) {
        this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CardsCategoryPage');
    }

    ngOnInit(): void {
        this.loading.present();
        this.getPacketCateoory();
        this.getListMyPacket();
    }

    search() {
        let modal = this.modalCtrl.create(SearchPage, { param: this.packetCategory });
        modal.present();
    }

    category(id: number, name_category: string) {
        this.navCtrl.push(CategoryPage, { id_packet_category_master: id, name_category: name_category });
    }
    notification() {
        this.navCtrl.push(NotificationPage);
    }

    getPacketCateoory() {
        this.packet_category.get().subscribe(
            value => {
                this.packetCategory = value.data;
            }
        );
    }

    goToMyApacket() {
        this.navCtrl.push(OngoingcoursesPage);
    }
    detailPacket(id: any) {
        this.navCtrl.push(MyCourcedetails, { id_packet: id });
    }

    getListMyPacket() {
        this.myPacketProv.get({ limit: 2 }).subscribe(
            value => {
                this.dataMyPacket = value.data;
                this.loading.dismiss();
            }
        );
    }




}
