import { Component, HostListener } from '@angular/core';
import { AlertController, LoadingController, MenuController, NavController, NavParams } from 'ionic-angular';

import { HomePage } from '@pages/home/home';
import { SignupPage } from '@pages/signup/signup';
import { AuthProvider } from '@providers/auth/auth';

@Component({
    selector: 'page-singin',
    templateUrl: 'singin.html'
})
export class SinginPage {
    loading: any;
    formLogin: any = {
        email: '',
        password: '',
    };

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private alertCtrl: AlertController,
        private loadingCtrl: LoadingController,
        private menuCtrl: MenuController,
        private authService: AuthProvider
    ) {
        this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
    }

    ionViewDidLoad() {
        this.checkAuthenticated();
    }

    async checkAuthenticated() {
        try {
            let isAuthenticated = await this.authService.checkIsAuthenticated();
            if (isAuthenticated) {
                this.menuCtrl.enable(true);
                this.navCtrl.setRoot(HomePage);
            }
        } catch (err) {
            const alert = this.alertCtrl.create({ title: 'Error', buttons: ['Ok'] });
            const errMessage = 'System error occurred. Please try again later.';

            alert.setMessage(errMessage);
            alert.present();
        }
    }

    @HostListener('document:keypress', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {
        if (event.key === 'Enter') {
            const validating = this.validateLoginData(this.formLogin);

            if (validating.isValid) {
                this.doLogin(this.formLogin);
            } else {
                const alert = this.alertCtrl.create({ title: 'Error', buttons: ['Ok'] });

                alert.setMessage(validating.message);
                alert.present();
            }
        }
    }

    submitForm(data: any) {
        const validating = this.validateLoginData(this.formLogin);

        if (validating.isValid) {
            this.doLogin(data);
        } else {
            const alert = this.alertCtrl.create({ title: 'Error', buttons: ['Ok'] });

            alert.setMessage(validating.message);
            alert.present();
        }
    }

    async doLogin(data: any) {
        this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
        this.loading.present();
        try {
            const response = await this.authService.login(data);

            this.loading.dismiss();
            this.authService.storeCredentials(response);
            setTimeout(() => {
                this.checkAuthenticated();
            }, 300);
        } catch (err) {
            setTimeout(() => {
                this.loading.dismiss();
                const alert = this.alertCtrl.create({ title: 'Error', buttons: ['Ok'] });
                const errMessage = 'System error occurred. Please try again later.';

                alert.setMessage(errMessage);
                alert.present();
            }, 500);
        }

    }

    validateLoginData(input: any) {
        const REGEX = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;

        if (!input.email) {
            return { isValid: false, message: 'Email cannot be empty.' };
        }

        if (!input.password) {
            return { isValid: false, message: 'Password cannot be empty.' };
        }

        if (input.password.length < 6) {
            return { isValid: false, message: 'Password at least 6 characters.' };
        }

        if (!REGEX.test(input.email)) {
            return { isValid: false, message: 'Please enter a valid email address.' };
        }

        return { isValid: true, message: '' };
    }


    home() {
        this.navCtrl.setRoot(HomePage);
    }

    signup() {
        this.navCtrl.setRoot(SignupPage);
    }

}
