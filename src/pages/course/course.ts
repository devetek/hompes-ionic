import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams } from 'ionic-angular';
import { BookProvider } from '@providers/book/book';
import { Book, Section } from '@models/book';
import { CourseReadingPage } from '@pages/courseReading/courseReading';

@Component({
    selector: 'page-course',
    templateUrl: 'course.html'
})
export class CoursePage {
    tab = 'lactures';

    id_book: number;
    loading: any;
    dataBook: Book;
    dataSection: Section[] = [];

    constructor(
        public navCtrl: NavController,
        public navParam: NavParams,
        public bookProv: BookProvider,
        private loadingCtrl: LoadingController,
    ) {
        this.id_book = parseInt(navParam.get('id_book'), 10) || 0;
        this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
    }
    ionViewDidLoad() {
        console.log('detail paket');
        this.loading.present();
        this.getBooks();
    }
    getBooks() {
        this.bookProv.get(this.id_book).subscribe(
            (value: { data: { book: Book; sections: Section[]; }; }) => {
                this.dataBook = value.data.book;
                this.dataSection = value.data.sections;
                this.loading.dismiss();
            }
        );
    }

    startReading(id: any) {
        this.navCtrl.push(CourseReadingPage, { id_book_content: id });
    }


}
