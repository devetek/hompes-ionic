import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams } from 'ionic-angular';
import { Content } from '@models/book';
import { BookContentProvider } from '@providers/book-content/book-content';
import { BookContentTicketProvider } from '@providers/book-content-ticket/book-content-ticket';
import { Ticket } from '@models/ticket';

@Component({
    selector: 'page-course',
    templateUrl: 'courseReading.html'
})
export class CourseReadingPage {
    tab = 'lactures';

    id_book_content: number;
    loading: any;
    dataContent: Content;
    dataTicket: Ticket[] = [];

    constructor(
        public navCtrl: NavController,
        public navParam: NavParams,
        public bookContentProv: BookContentProvider,
        public bookTicketProv: BookContentTicketProvider,
        private loadingCtrl: LoadingController,
    ) {
        this.id_book_content = navParam.get('id_book_content');
        this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
    }
    ionViewDidLoad() {
        console.log('detail paket');
        this.loading.present();
        this.getContent();
        this.getTicket();
    }
    getContent() {
        this.bookContentProv.get(this.id_book_content).subscribe(
            value => {
                this.dataContent = value.data;
                this.loading.dismiss();
            }
        );
    }
    getTicket() {
        this.bookTicketProv.get({ id_book_content: this.id_book_content, for_module: 'packet', sub_module_id: this.id_book_content }).subscribe(
            value => {
                this.dataTicket = value.data;
            }
        );
    }


}
