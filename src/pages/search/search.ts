import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { DesignPage } from '@pages/design/design';
import { PacketCategory } from '@providers/packet-category/packet-category';

@Component({
    selector: 'page-search',
    templateUrl: 'search.html'
})
export class SearchPage {
    searchInput = '';
    packetCategory: PacketCategory[] = [];

    constructor(
        public navCtrl: NavController,
        public navParam: NavParams,
        public viewCtrl: ViewController
    ) {
        this.packetCategory = navParam.get('param');
    }

    getItems(event: any) {
        if (event.key === 'Enter' && this.searchInput.length > 3) {
            // TODO: Search flow do here
            console.log('=== |data input and keyCode| ===', { keyCode: event.key, searchInput: this.searchInput });
        }
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    design() {
        this.navCtrl.push(DesignPage);
    }

}
