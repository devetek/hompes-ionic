import { Component } from '@angular/core';
import { NavController, ModalController, LoadingController } from 'ionic-angular';

import { CourcedetailsPage } from '@pages/courcedetails/courcedetails';
import { FiltersPage } from '@pages/filters/filters';
import { NavParams } from 'ionic-angular';
import { PacketProvider } from '@providers/packet/packet';
import { Packets } from '@models/pakcet';

@Component({
    selector: 'page-category',
    templateUrl: 'category.html'
})
export class CategoryPage {

    id_packet_category_master: number;
    name_category: string;
    loading: any;
    dataPackets: Packets[] = [];
    constructor(
        public navCtrl: NavController,
        public modalCtrl: ModalController,
        public packetProv: PacketProvider,
        private loadingCtrl: LoadingController,
        public navParam: NavParams
    ) {
        this.id_packet_category_master = navParam.get('id_packet_category_master');
        this.name_category = navParam.get('name_category');
        this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
        console.log(this.id_packet_category_master);
    }

    filters() {
        let modal = this.modalCtrl.create(FiltersPage);
        modal.present();
    }

    courcedetails(id) {
        this.navCtrl.push(CourcedetailsPage, { id_packet: id });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad EmiCalPage');
        this.getPacketByCategory();
    }

    getPacketByCategory() {
        this.loading.present();
        this.packetProv.get({ id_packet_category_master: this.id_packet_category_master }).subscribe(
            value => {
                this.dataPackets = value.data;

                this.loading.dismiss();
            }
        );
    }


}
