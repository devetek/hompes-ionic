import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { LecturerprofilePage } from '@pages/lecturerprofile/lecturerprofile';
import { Packets } from '@models/pakcet';
import { Packet_goal } from '@models/packet_goal';
import { Packet_detail } from '@models/packet_detail';
import { Packet_requirement } from '@models/packet_requirement';
import { PacketProvider } from '@providers/packet/packet';
import { PacketBookProvider } from '@providers/packet-book/packet-book';
import { Packet_book } from '@models/packet_book';

@Component({
    selector: 'page-courcedetails',
    templateUrl: 'courcedetails.html'
})
export class CourcedetailsPage {
    tab = 'about';

    id_packet: number;
    dataPacket: Packets;
    dataGoal: Packet_goal;
    dataDetail: Packet_detail;
    dataRequirement: Packet_requirement;
    dataBooks: Packet_book[] = [];

    constructor(
        public navCtrl: NavController,
        public packetProv: PacketProvider,
        public packetBookProv: PacketBookProvider,
        public navParam: NavParams
    ) {
        this.id_packet = navParam.get('id_packet');
    }
    ionViewDidLoad() {
        console.log('detail paket');
        this.getDetail();
        this.getListBooks();
    }
    getDetail() {
        this.packetProv.detail(this.id_packet).subscribe(
            value => {
                this.dataPacket = value.data.packet;
                this.dataDetail = value.data.packet_detail;
                this.dataGoal = value.data.packet_goal;
                this.dataRequirement = value.data.packet_requirement;
            }
        );
    }

    getListBooks() {
        this.packetBookProv.get({ id_packet: this.id_packet }).subscribe(
            value => {
                this.dataBooks = value.data;
            }
        );
    }


    lecturerprofile() {
        this.navCtrl.push(LecturerprofilePage);
    }

}
