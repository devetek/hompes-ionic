import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CourcedetailsPage } from '@pages/courcedetails/courcedetails';
@Component({
  selector: 'page-bookmarked',
  templateUrl: 'bookmarked.html'
})
export class BookmarkedPage {
  constructor(public navCtrl: NavController) {

  }

  courcedetails() {
    this.navCtrl.push(CourcedetailsPage);
  }

}
