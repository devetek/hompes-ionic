import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams } from 'ionic-angular';

import { LecturerprofilePage } from '@pages/lecturerprofile/lecturerprofile';
import { Packet_goal } from '@models/packet_goal';
import { Packet_detail } from '@models/packet_detail';
import { Packet_requirement } from '@models/packet_requirement';
import { PacketProvider } from '@providers/packet/packet';
import { PacketBookProvider } from '@providers/packet-book/packet-book';
import { Packet_book } from '@models/packet_book';
import { Mypakcet } from '@models/mypakcet';
import { PacketGoalsProvider } from '@providers/packet-goals/packet-goals';
import { PacketDetailsProvider } from '@providers/packet-details/packet-details';
import { PacketRequirementsProvider } from '@providers/packet-requirements/packet-requirements';
import { CoursePage } from '@pages/course/course';

@Component({
    selector: 'page-courcedetails',
    templateUrl: 'myCourcedetails.html'
})
export class MyCourcedetails {
    tab = 'videos';

    id_packet: number;
    dataPacket: Mypakcet;
    dataGoal: Packet_goal[];
    dataDetail: Packet_detail[];
    dataRequirement: Packet_requirement[];
    dataBooks: Packet_book[] = [];

    loading: any;
    constructor(
        public navCtrl: NavController,
        public packetProv: PacketProvider,
        public packetDetailProv: PacketDetailsProvider,
        public packetGoalProv: PacketGoalsProvider,
        public packetRequirementProv: PacketRequirementsProvider,
        public packetBookProv: PacketBookProvider,
        private loadingCtrl: LoadingController,
        public navParam: NavParams
    ) {
        this.id_packet = navParam.get('id_packet');
        this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
    }
    ionViewDidLoad() {
        console.log('detail paket');
        this.loading.present();
        this.getPacket();
        this.getListBooks();
        this.getGoals();
        this.getRequirement();
        this.getDetail();
    }
    getPacket() {
        this.packetProv.mydetail(this.id_packet).subscribe(
            value => {
                this.dataPacket = value.data;
            }
        );
    }
    getDetail() {
        this.packetDetailProv.get({ id_packet: this.id_packet }).subscribe(
            value => {
                this.dataDetail = value.data;
            }
        );
    }
    getGoals() {
        this.packetGoalProv.get({ id_packet: this.id_packet }).subscribe(
            value => {
                this.dataGoal = value.data;
            }
        );
    }
    getRequirement() {
        this.packetRequirementProv.get({ id_packet: this.id_packet }).subscribe(
            value => {
                this.dataRequirement = value.data;
            }
        );
    }

    getListBooks() {
        this.packetBookProv.get({ id_packet: this.id_packet }).subscribe(
            value => {
                this.dataBooks = value.data;
                this.loading.dismiss();
            }
        );
    }


    // REDIRECT
    lecturerprofile() {
        this.navCtrl.push(LecturerprofilePage);
    }

    toBookDetail(id) {
        this.navCtrl.push(CoursePage, { id_book: id });
    }

}
