import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { CourcedetailsPage } from '@pages/courcedetails/courcedetails';
@Component({
  selector: 'page-lecturerprofile',
  templateUrl: 'lecturerprofile.html'
})
export class LecturerprofilePage {
  tab = 'about';
  constructor(public navCtrl: NavController) {

  }

  courcedetails() {
    this.navCtrl.push(CourcedetailsPage);
  }

}
