import { Component } from '@angular/core';
import { LoadingController, NavController } from 'ionic-angular';

import { MypacketProvider } from '@providers/mypacket/mypacket';
import { Mypakcet } from '@models/mypakcet';
import { MyCourcedetails } from '@pages/myCourcedetails/myCourcedetails';

@Component({
    selector: 'page-ongoingcourses',
    templateUrl: 'ongoingcourses.html'
})
export class OngoingcoursesPage {

    dataMyPacket: Mypakcet[] = [];
    loading: any;

    constructor(
        public navCtrl: NavController,
        private loadingCtrl: LoadingController,
        public myPacketProv: MypacketProvider
    ) {
        this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
    }

    ionViewDidLoad() {
        console.log('detail paket');
        this.loading.present();
        this.getListMyPacket();
    }

    detailPacket(id) {
        this.navCtrl.push(MyCourcedetails, { id_packet: id });
    }

    getListMyPacket() {
        this.myPacketProv.get({}).subscribe(
            value => {
                this.dataMyPacket = value.data;
                this.loading.dismiss();
            }
        );
    }

}
