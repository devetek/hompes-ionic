import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { HomePage } from '@pages/home/home';
import { SinginPage } from '@pages/singin/singin';
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {

  constructor(public navCtrl: NavController) {

  }

  home() {
    this.navCtrl.setRoot(HomePage);
  }

  singin() {
    this.navCtrl.setRoot(SinginPage);
  }


}
