const path = require('path');
const appRootDir = require('app-root-dir');

const pathResolver = (dirPath = '') => {
  return path.resolve(appRootDir.get(), dirPath);
};

module.exports = { pathResolver };
